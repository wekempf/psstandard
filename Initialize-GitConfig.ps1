<#
.SUMMARY
    Initializes the standard git configuration.

.PARAMETER Force
    Force initialization even if it's been done before.
#>
function Initialize-GitConfig {
    [CmdletBinding()]
    param(
        [switch]$Force
    )

    function Get-Value([string]$entry) {
        $entry.Substring($entry.IndexOf('=') + 1)
    }

    $cfg = git config --global -l

    # Always ensure user information is initialized.
    $entry = $cfg | Select-String "user.name="
    if ($Force -or (-not $entry)) {
        if ($entry) {
            $value = Get-Value $entry.Line
        }
        while ($True) {
            Write-Host -NoNewline "What is your git user name? "
            if ($value) {
                Write-Host -NoNewline "(Default: $value) "
            }
            $input = Read-Host
            if (-not $input) {
                $input = $value
                if (-not $input) {
                    Write-Warning "You must supply a user name."
                    continue
                }
            }
            git config --global user.name $input
            break
        }
    }
    $entry = $cfg | Select-String "user.email="
    if ($Force -or (-not $entry)) {
        if ($entry) {
            $value = Get-Value $entry.Line
        }
        while ($True) {
            Write-Host -NoNewline "What is your git email address? "
            if ($value) {
                Write-Host -NoNewline "(Default: $value) "
            }
            $input = Read-Host
            if (-not $input) {
                $input = $value
                if (-not $input) {
                    Write-Warning "You must supply an email address."
                    continue
                }
            }
            git config --global user.email $input
            break
        }
    }

    # Everything else does not get initialized if we've already initialized the git config once.
    # Force can be used to force initialization if this script changes.
    $entry = $cfg | Select-String "psstandard.init="
    if ($Force -or (-not $entry)) {
        if (Find-Location p4merge.exe -ErrorAction SilentlyContinue) {
            git config --global merge.tool p4merge
            git config --global mergetool.p4merge.cmd 'p4merge "$BASE" "$LOCAL" "$REMOTE" "$MERGED"'
        } else {
            Write-Warning "Merge tool p4merge not found in the path."
        }
        git config --global alias.ci commit
        git config --global alias.st status
        git config --global alias.co checkout
        git config --global alias.br branch
        git config --global alias.unstage 'reset HEAD --'
        git config --global alias.last 'log -1 HEAD'
        git config --global alias.glog 'log --oneline --abbrev-commit --all --graph'
        git config --global core.editor notepad.exe
        git config --global push.default simple

        git config --global psstandard.init true
    }
}

Export-ModuleMember -Function Initialize-GitConfig