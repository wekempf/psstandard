﻿Set-StrictMode -Version 3

$ModuleRoot = $PSScriptRoot

$NamedLocationsFile = (Join-Path (Split-Path $Profile) "NamedLocations.xml")
if (Test-Path $NamedLocationsFile) {
    $NamedLocations = Import-Clixml $NamedLocationsFile
} else {
    $NamedLocations = @{ "Home" = (Resolve-Path ~) }
}

New-PSDrive -Name HKCR -PSProvider Registry -Root HKEY_CLASSES_ROOT

# Load the various functions.
. $ModuleRoot\Add-NamedLocation.ps1
. $ModuleRoot\Add-PathVariable.ps1
. $ModuleRoot\Edit-File.ps1
. $ModuleRoot\Edit-Profile.ps1
. $ModuleRoot\Expand-Template.ps1
. $ModuleRoot\Find-Location.ps1
. $ModuleRoot\Get-AssociationType.ps1
. $ModuleRoot\Get-FileAssociation.ps1
. $ModuleRoot\Get-NamedLocation.ps1
. $ModuleRoot\Get-Option.ps1
. $ModuleRoot\Get-ProfilePath.ps1
. $ModuleRoot\Get-ShortPathName.ps1
. $ModuleRoot\Initialize-GitConfig.ps1
. $ModuleRoot\Invoke-VsVars32.ps1
#. $ModuleRoot\Install-Module.ps1
. $ModuleRoot\New-Password.ps1
. $ModuleRoot\Remove-NamedLocation.ps1
. $ModuleRoot\Set-HostColor.ps1
. $ModuleRoot\Set-ModifiedTime.ps1
. $ModuleRoot\Set-NamedLocation.ps1
. $ModuleRoot\Test-Admin.ps1
. $ModuleRoot\Wait-ForKeyPress.ps1

$noProfile = $args | Where-Object { $_ -eq 'NoProfile' }
if (-not $noProfile) {
# Load the standard profile settings.
. $ModuleRoot\Profile.ps1
}
