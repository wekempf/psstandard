<#
.SYNOPSIS
    Adds a named location alias.

.PARAMETER Name
    The location name or alias.

.PARAMETER Path
    The location.
#>
function Add-NamedLocation {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True, Position=0)]
        $Name,

        [Parameter(Mandatory=$True, Position=1)]
        $Path
    )

    Begin {
        trap { ; break }
    }

    Process {
        if ($Name.Contains('/') -or $Name.Contains('\')) {
            throw "Invalid name."
        }
        $NamedLocations.Add($Name, (Resolve-Path $Path))
        $NamedLocations | Export-Clixml $NamedLocationsFile -Force
    }

    End {
    }
}

Export-ModuleMember -Function Add-NamedLocation