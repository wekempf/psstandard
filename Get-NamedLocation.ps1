<#
.SYNOPSIS
    Gets a named location.

.PARAMETER Name
    The location name or alias.
#>
function Get-NamedLocation {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$False)]
        $Name
    )

    if ($Name) {
        if (-not $NamedLocations.Contains($Name)) {
            throw "Location named '$Name' not found."
        }

        New-Object PSObject -Property @{ Name = $Name; Location = $NamedLocations[$Name] }
    } else {
        $NamedLocations.GetEnumerator() | Sort-Object Name | ForEach-Object { New-Object PSObject -Property @{ Name = $_.Name; Location = $_.Value } }
    }
}

Export-ModuleMember -Function Get-NamedLocation