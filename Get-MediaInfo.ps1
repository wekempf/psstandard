[System.Reflection.Assembly]::LoadFile("$PSScriptRoot\taglib-sharp.dll")

function Get-MediaInfo {
	<#
	.SYNOPSIS
	USER cmdlet: Gets the media information from an MP3 file.

	.DESCRIPTION
	Gets the media information from an MP3 file.

	.PARAMETER Path
	The path to the MP3 file.
	#>

	param(
        [Parameter(ValueFromPipelineByPropertyName=$True)]
        [Alias('FullName')]
		[string]$Path)
        
    [taglib.File]::Create($Path)
}

Export-ModuleMember -Function Get-MediaInfo