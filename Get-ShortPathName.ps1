<#
.SYNOPSIS
    Gets a shortened path name.

.DESCRIPTION
    Gets a shortened path name intended for display use only. The result is no longer a valid path.

.PARAMETER Path
    The path to shorten.
#>
function Get-ShortPathName {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        $Path
    )

    $Path = Resolve-Path $Path
    # Shorten the Home directory portion
    $Path = $Path -replace [RegEx]::Escape($Home), '~'
    # Remove prefix for UNC paths
    $Path = $Path -replace '^[^:]+::', ''
    # Make path shorter
    $Path = $Path -replace '\\(\.?)([^\\])[^\\]*(?=\\)','\$1$2'
    $Path
}

Export-ModuleMember -Function Get-ShortPathName