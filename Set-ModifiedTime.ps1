<#
.Synopsis
   Sets the modified time for the specified file.
#>
function Set-ModifiedTime
{
    [CmdletBinding(DefaultParameterSetName='date')]
    [OutputType([int])]
    Param
    (
        # The file or directory to update.
        [Parameter(Mandatory=$true,
                   ValueFromPipeline=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        [string[]]
        $Path,

        # The date/time to set.
        [Parameter(Mandatory=$false,
                   ParameterSetName='date',
                   Position=1)]
        [DateTime]
        $Time = ([DateTime]::Now),

        # The directory/file to get the date from.
        [Parameter(Mandatory=$true,
                   ParameterSetName='reference',
                   Position=1)]
        [string]
        $Reference,

        [switch]
        $AccessTime,

        [switch]
        $LastWrite,

        # Create an empty file if the path doesn't exist.
        [switch]
        $Force
    )

    Begin {
        if ($PSCmdlet.ParameterSetName -eq 'reference') {
            $Time = (Get-Item $Reference -ErrorAction Stop).LastWriteTime
        }
        if ((-not $AccessTime) -and (-not $LastWrite)) {
            $AccessTime = $LastWrite = $true
        }
    }
    Process {
        $Path | ForEach-Object {
            if (-not (Test-Path $_)) {
                if (-not $Force) {
                    throw "'$_' does not exist. Use -Force to create empty file."
                } else {
                    New-Item -Path $_ -ItemType File | Out-Null
                }
            } 
            $item = Get-Item $_ -ErrorAction Stop
            if ($AccessTime) {
                $item.LastAccessTime = $Time
            }
            if ($LastWrite) {
                $item.LastWriteTime = $Time
            }
        }
    }
}
Set-Alias -Name touch -Value Set-ModifiedTime
Export-ModuleMember -Function Set-ModifiedTime -Alias touch