﻿<#
.SYNOPSIS
    Expands a template script.
	
.DESCRIPTION
    Expand-Template expands a PowerShell Template Script (typically given a pst1 extension).
	
.PARAMETER Template
    The template to expand.
    
    If a script block is passed as the template it will be invoked and each object supplied to the output stream will be converted to a string and joined.
    
    If a file is specified it will be converted into a script block and processed. The script file is searched for along the TemplatePath path.
	
.PARAMETER Arguments
    A hashtable of arguments to supply to the template.
	
.PARAMETER FilePath
    Specifies the path to the output file.
    
.PARAMETER Encoding
    Specifies the encoding for the ouptut file.
	
.PARAMETER Append
    Adds the output to the end of an existing file, instead of replacing the file contents.
    
.PARAMETER Force
    Allows the cmdlet to overwrite an existing read-only file. Even using the Force parameter, the cmdlet cannot override security restrictions.
	
.EXAMPLE
    PS> Expand-Template .\mytemplate.pst1
    Results of mytemplate.
    
    Description
    -----------
    Expands the 'mytemplate.pst1' template located in the current directory.
    
.EXAMPLE
    PS> Expand-Template mytemplate
    Results of mytemplate.
    
    Description
    -----------
    Expands the 'mytemplate.pst1' template found in one of the paths specified in the 'TemplatePath' variable.
    
.EXAMPLE
    PS> Expand-Template func @{ Name = "Get-Foo"; Parameter = "Bar" } -FilePath Get-Foo.ps1
    
    Description
    -----------
    Expands the 'func.pst1' template (this is a standard template included in the module) found in one of the paths specified in the 'TemplatePath' variable, passing the 'Name' and 'Parameter' arguments.
#>
function Expand-Template {
	[CmdletBinding(
		SupportsShouldProcess=$True,
		SupportsTransactions=$False,
		ConfirmImpact="None",
		DefaultParameterSetName="")]
	param(
		[Parameter(Position = 1, Mandatory = $True)]
		$Template,
	
		[Parameter(Position = 2)]
        [hashtable]
		$Arguments,
	
		[Parameter()]
		$FilePath,
        
        [Parameter()]
        [string]
        $Encoding = "ASCII",
	
		[Parameter()]
        [switch]
		$Append,
        
        [Parameter()]
        [switch]
        $Force
	)

	Begin {
        trap { ; break }
	}
	
	Process {
        if ($Arguments) {
            foreach ($argname in $Arguments.Keys) {
                set-variable -name $argname -value $Arguments[$argname]
            }
        }

	    if (Test-Path variable:\TemplatePath) {        
	            $TemplatePath += ";$ModuleRoot\templates"
	    } else {
		    $TemplatePath = "$ModuleRoot\templates"
	    }
        
        if (-not $($Template -is [scriptblock])) {
            if (-not $(test-path $Template)) {
                $paths = $TemplatePath.Split(";")
                foreach ($path in $paths) {
                    if (-not $path) {
                        continue
                    }
                    
                    $path = join-path $path $Template
                    if (-not $(test-path $path)) {
                        $path = $path + ".pst1"
                    }
                    
                    if (test-path $path) {
                        $Template = $path
                        break
                    }
                }
            }
            
            if (-not $(test-path $Template)) {
                throw "Cannot find template '$Template'"
                return
            }
            
            function CreateScriptBlock($text) {
            }
            
            $content = get-content $Template
            $content = [string]::Join("`n", @($content))
            $Template = [ScriptBlock]::Create($content)
        }
        
        $result = $(& $Template)
        $result = [string]::Join("`n", @($result))
        $result = [regex]::Replace($result, "((?<!`r)`n)|(`r(?!`n))", "`r`n")
        if ($FilePath) {
            $result | out-file $FilePath -Append:$Append -Force:$Force -Encoding:$Encoding
        } else {
            return $result
        }
    }
	
	End {
	}
}

Export-ModuleMember -Function Expand-Template
