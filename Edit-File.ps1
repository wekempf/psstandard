﻿<#
.SYNOPSIS
    Edits a file.

.DESCIPTION
    Edits a file using the PowerShell ISE if the file is a PowerShell file, or with $env:EDITOR or Notepad.exe.

.PARAMETER Path
    The path to the file to edit.
#>
function Edit-File {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True,
                   ValueFromPipeline=$true,
                   ValueFromPipelineByPropertyName=$True,
                   Position=0)]
        [Alias("FullName","FileName")]
        [string[]]
        $Path
    )

    process {
        foreach ($file in $Path) {
            Write-Verbose "Edit-File '$file'"
            if (-not (Test-Path $file)) {
                Write-Warning "'$file' not found."
                $opt = Get-Option ('Yes',"Create '$file'"),('No',"Do not create '$file'") -Message "Create '$file'?" -DefaultChoice 0
                if ($opt -ne 'Yes') {
                    Write-Warning "Skipping '$file'."
                    continue;
                }
                New-Item -ItemType File $file | Out-Null
            }
            $file = Get-Item $file
            $extension = $file.Extension
            if ($extension -and (Get-FileAssociation $extension 'edit')) {
                Write-Verbose 'Using ShellExecute'
                $shell = New-Object -ComObject Shell.Application
                $shell.ShellExecute($file, '', $file.DirectoryName, 'edit')
            } else {
                if ($extension) {
                    $iseExtensions = @('.ps1', '.psm1', '.psd1', '.ps1xml', '.pssc', '.cdxml', '.help.txt')
                    if ($iseExtensions | Where-Object { $file.FullName.EndsWith($_) }) {
                        Write-Verbose 'Using ISE'
                        if ($Host.Name -match "\sISE\s") {
                            $psISE.CurrentPowerShellTab.Files.Add($file) | Out-Null
                            return
                        }
                        ise $file
                        return
                    }
                }
                Write-Verbose 'Using env:\EDITOR'
                $editor = $env:EDITOR
                if (-not $editor) {
                    Write-Verbose 'Using notepad.exe'
                    $editor = "notepad"
                }
                & $editor $file
            }
        }
    }
}
Set-Alias -Name edit -Value Edit-File
Export-ModuleMember -Function Edit-File -Alias edit