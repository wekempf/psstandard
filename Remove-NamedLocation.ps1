<#
.SYNOPSIS
    Removes a named location alias.

.PARAMETER Name
    The location name or alias.
#>
function Remove-NamedLocation {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True, Position=0)]
        $Name
    )

    Begin {
        trap { ; break }
    }

    Process {
        $NamedLocations.Remove($Name)
        $NamedLocations | Export-Clixml $NamedLocationsFile -Force
    }

    End {
    }
}

Export-ModuleMember -Function Remove-NamedLocation