<#
.SYNOPSIS
    Generate random and secure passwords that are pronouncable.

.PARAMETER MinimumLength
    The minuimum length of the generated passwords.

.PARAMETER MaximumLength
    The maximum length of the generated passwords.

.PARAMETER Number
    The number of passwords to generate.
#>
function New-Password {
    [CmdletBinding()]
	param(
        [Parameter(Position=1, Mandatory=$False)]
		[int]$MinimumLength = 6,

        [Parameter(Position=2, Mandatory=$False)]
		[int]$MaximumLength = 12,

        [Parameter(Position=3, Mandatory=$False)]
		[int]$Number = 1
    )

	$dict = join-path $ModuleRoot dict.csv
	$rand = new-object System.Random
	$words = @{}
	$tris = @{}
	foreach ($word in import-csv $dict) {
		$word = $word.word.ToUpper()
		$words.Add($word, 1)
		for ($i = 0; $i -lt $word.Length - 2; ++$i) {
			$tri = $word.Substring($i, 3)
			if ($tris.Contains($tri)) {
				$tris.Item($tri)++
			} else {
				$tris.Add($tri, 1)
			}
		}
	}

	function Generate-Password($MinimumLength, $MaximumLength) {
		$targetlen = $rand.Next(0, $MaximumLength - $MinimumLength) + $MinimumLength
		$start = ""
		$lastlen = 0
		for ($pw = ""; $pw.Length -lt $targetlen;) {
			if ($pw.Length -eq 0) {
				$ctris = $tris
				$sigma = 0
				foreach ($tri in $tris.Keys) {
					$sigma += $tris[$tri]
				}
			} else {
				$ctris = @{}
				$sigma = 0
				$duo = $pw.Substring($pw.Length - 2)
				for ($i = 0; $i -lt 26; ++$i) {
					$tri = $duo + [char]([int][char]'A' + $i)
					if ($tris.Contains($tri)) {
						$ctris.Add($tri, $tris[$tri])
						$sigma += $tris[$tri]
					}
				}
			}
			$r = $rand.Next(0, $sigma)
			$sum = 0
			foreach ($tri in $ctris.Keys) {
				$sum += $ctris[$tri]
				if ($sum -ge $r) {
					if ($pw.Length -eq 0) {
						$pw = $tri
					} else {
						$pw += $tri.Substring($tri.Length - 1)
					}
					break
				}
			}
			if ($pw.Length -eq $lastlen) {
				return Generate-Password $MinimumLength $MaximumLength
			}
			$lastlen = $pw.Length
		}
		if ($words.Contains($pw)) {
			return Generate-Password $MinimumLength $MaximumLength
		}
		return $pw
	}

	$found = @{ }
	for ($i = 0; $i -lt $Number; ++$i) {
		while ($true) {
			$pass = Generate-Password $MinimumLength $MaximumLength
			if (-not $found.Contains($pass)) {
				$found.Add($pass, $true)
				$pass
				break
			}
		}
	}
}

Export-ModuleMember -Function New-Password