<#
.SYNOPSIS
    Sets the host color.

.PARAMETER ForegroundColor
    Sets the foreground color.

.PARAMETER BackgroundColor
    Sets the background color.

.PARAMETER Clear
    Clears the host.
#>
function Set-HostColor {
    [CmdletBinding()]
    param(
        [Parameter(Position=1)]
        $BackgroundColor,

        [Parameter(Position=2)]
        $ForegroundColor,

        [Parameter()]
        [switch]$Clear
    )

    if ($ForegroundColor -or $BackgroundColor) {
        $ui = $Host.UI.RawUI
        $ise = $psISE.Options
        if ($ForegroundColor) {
            $ui.ForegroundColor = $ForegroundColor
        }
        if ($BackgroundColor) {
            $ui.BackgroundColor = $BackgroundColor
            if ($ise) {
                $ise.ConsolePaneBackgroundColor = $BackgroundColor
            }
        }
        if ($Clear) {
            Clear-Host
        }
    } else {
        throw "No color specified."
    }
}

Export-ModuleMember -Function Set-HostColor