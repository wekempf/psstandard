<#
.Synopsis
   Short description
.DESCRIPTION
   Long description
.EXAMPLE
   Example of how to use this cmdlet
.EXAMPLE
   Another example of how to use this cmdlet
#>
function Get-AssociationType
{
    [CmdletBinding()]
    [OutputType([int])]
    Param
    (
        # Param1 help description
        [Parameter(Mandatory=$true,
                   ValueFromPipeline=$true,
                   Position=0)]
        [Alias("Extension")]
        [string[]]
        $FileExtension
    )

    Begin
    {
    }
    Process
    {
        $global:Foo = 'bar'
        $FileExtension | ForEach-Object {
            $p = (Get-ItemProperty HKCR:\$_ '(default)' -ErrorAction SilentlyContinue)
            if ($p) {
                $p.'(default)'
            }
        }
    }
    End
    {
    }
}
Set-Alias -Name ftype -Value Get-AssociationType
Export-ModuleMember -Function Get-AssociationType -Alias ftype