<#
.SYNOPSIS
    Allows the user to pick from various options.

.PARAMETER Choices
    An Array of Choices, like Yes, No and Maybe.

.PARAMETER Caption
    The text that precedes (a title) the choices.

.PARAMETER Message
    A message that describes the choice.

.PARAMETER DefaultChoice
    The index of the choice in the $Choices collection for the default choice.

.EXAMPLE
    PS> Get-Option -Choices ('Yes','Continue.'),('No','Do not continue.') -Caption "Do you want to continue?"
#>
function Get-Option {
    [CmdletBinding()]
    param(
        [Parameter(Position=0, Mandatory=$True, ValueFromPipeline=$True)]
        $Choices,

        [Parameter(Position=1)]
        $Caption,
        
        [Parameter(Position=2)]
        $Message,
        
        [Parameter(Position=3)]
        [int]$DefaultChoice=-1
    )

    $descriptions = $Choices | ForEach-Object {
        if ($_ -is [array]) {
            $label = "&" + $_[0]
            $help = $_[1]
            @(New-Object System.Management.Automation.Host.ChoiceDescription($label, $help))
        } else {
            @(New-Object System.Management.Automation.Host.ChoiceDescription("&" + $_))
        }
    }
    $i = $Host.UI.PromptForChoice($Caption, $Message, $descriptions, $DefaultChoice)
    $result = $Choices[$i]
    if ($result -is [array]) {
        $result[0]
    } else {
        $result
    }
}

Export-ModuleMember -Function Get-Option