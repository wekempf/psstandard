<#
.Synopsis
   Short description
.DESCRIPTION
   Long description
.EXAMPLE
   Example of how to use this cmdlet
.EXAMPLE
   Another example of how to use this cmdlet
#>
function Get-FileAssociation
{
    [CmdletBinding(DefaultParameterSetName='extension')]
    [OutputType([int])]
    Param
    (
        # Param1 help description
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   ParameterSetName='extension',
                   Position=0)]
        [Alias('Extension')]
        [string[]]
        $FileExtension,

        # Param2 help description
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   ValueFromPipeline=$true,
                   ParameterSetName='type',
                   Position=0)]
        [Alias('Type')]
        [string[]]
        $AssociationType,

        [Parameter(Mandatory=$false,
                   Position=1)]
        [string]
        $Command = 'open'
    )

    Begin
    {
    }
    Process
    {
        if ($PSCmdlet.ParameterSetName -eq 'extension') {
            $AssociationType = $FileExtension | ForEach-Object { Get-AssociationType $_ }
        }
        $AssociationType | ForEach-Object {
            $p = (Get-ItemProperty HKCR:\$_\shell\$Command\Command '(default)' -ErrorAction SilentlyContinue)
            if ($p) {
                $p.'(default)'
            }
        }
    }
    End
    {
    }
}
Set-Alias -Name assoc -Value Get-FileAssociation
Export-ModuleMember -Function Get-FileAssociation -Alias assoc