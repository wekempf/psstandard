function Invoke-VsVars32 {
	<#
	.SYNOPSIS
	SHARED cmdlet: Invokes the vsvars32.bat batch file in order to set the environment for using Visual Studio.
	
	.DESCRIPTION
	Invokes the vsvars32.bat batch file in order to set the environment for using Visual Studio.
	
	.PARAMETER Sku
	The Visual Studio Sku to use.
	
	.PARAMETER Persist
	Persists the Sku selection across PowerShell sessions.
	
	.EXAMPLE
	PS> Invoke-VsVars32
	
	.NOTES
	NAME: Invoke-VsVars32
	AUTHOR: William.Kempf
	LASTEDIT: 3/23/2010
	
	#Requires -Version 2.0
	#>
	
	[CmdletBinding(
		SupportsShouldProcess=$False,
		SupportsTransactions=$False,
		ConfirmImpact="None",
		DefaultParameterSetName="")]
	param(
		[Parameter(Position = 0)]
        [int]
		$Sku = 12,
	
		[Parameter()]
        [switch]
		$Persist
	)
	
	Begin {
	}
	
	Process {
    	$tools = $(gci "env:\vs$($Sku)0comntools").Value
    	if (-not $(test-path $tools)) {
    		throw "Sku '$Sku' is not installed"
    	}

    	if (-not $tools) {
    		$tools = $(gci env:\vs*comntools | sort-object { $_.Name })
    		if (-not $tools) {
    			throw "Visual Studio is not installed"
    		}
    		if ($tools -is [array]) {
    			$tools = $tools[0]
    		}
    		$tools = $tools.Value
    	}

    	$path = join-path $tools vsvars32.bat
    	if (-not $(test-path $path)) {
    		throw "Unable to find batch file '${path}'"
    	}

        $cmd = "`"$path`" & set"
        cmd /c $cmd | ForEach-Object {
            $p, $v = $_.Split('=')
            Set-Item -Path env:$p -Value $v
        }
	}
	
	End {
	}
}

Set-Alias -Name vs -Value Invoke-VsVars32
Export-ModuleMember -Function Invoke-VsVars32 -Alias VsVars
