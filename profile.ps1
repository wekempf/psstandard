$ProfileDir = Split-Path $Profile -parent
$ModuleDir = Join-Path $ProfileDir "modules"
$ScriptsDir = Join-Path $ProfileDir "scripts"
$LocalScriptsDir = Join-Path $ProfileDir "local_scripts"

$pause = $false
$ErrorActionPreference = 'Stop'
trap { $pause = $true; continue }
function Inform($Text) {
    Write-Warning $Text
    Set-Variable -Name pause -Value $true -Scope 1
}

function prompt {
    $realLASTEXITCODE = $LASTEXITCODE
    $Host.UI.RawUI.WindowTitle = "PS $(Get-Location)"
    Write-Host -NoNewline "$(Get-Location)"
    if (Get-Command Write-VcsStatus) {
        Write-VcsStatus
    }
    Write-Host -NoNewline '>'
    ' '
    $LASTEXITCODE = $realLASTEXITCODE
}

# If Git is installed we'll do a lot more.
if (Get-Command git) {
#    Initialize-GitConfig
    # Ensure PoshGet is loaded
    if (-not (Get-Module PoshGet)) {
        Import-Module PoshGet -ErrorAction SilentlyContinue | Out-Null
        if (-not (Get-Module PoshGet)) {
            Inform 'Cannot import PoshGet.'
        }
    }
} else {
    Inform "Git not installed or not in path."
}

if (Get-Module PoshGet) {
    Install-Module psprompt -Import
    Install-Module Posh-Git -Import
}

if (Get-Module Posh-Git) {
    $Global:GitPromptSettings.WorkingForegroundColor = [ConsoleColor]::Red
    $Global:GitPromptSettings.WorkingBackgroundColor = [ConsoleColor]::Black
}

# Reset $Home
Set-Variable -Name Home -Scope Global -Value $env:USERPROFILE -force
(Get-PsProvider FileSystem).Home = $Home
Set-Location ~ | Out-Null

# Modify the path to include certain directories
if (Test-Path $ScriptsDir) {
    Add-PathVariable $ScriptsDir
}
if (Test-Path $LocalScriptsDir) {
    Add-PathVariable $LocalScriptsDir
}
if (Test-Path "C:\bin") {
    Add-PathVariable "C:\bin"
}
if (-not (Get-Command 'ssh-agent' -ErrorAction SilentlyContinue)) {
    $git = Get-Command git
    if ($git) {
        $gitBin = Join-Path (Split-Path (Split-Path $git.Path)) "bin"
        if (Test-Path $gitBin) {
            Add-PathVariable $gitBin
        }
    }
}

Start-SshAgent

if (Test-Path ~OneDrive\Apps\Console2) {
    $env:EDITOR = Resolve-Path ~\OneDrive\Apps\Console2\Console2.exe
}

if ($pause) {
    Wait-ForKeyPress
}

# Set the Host colors
Set-HostColor Black Green -Clear

Export-ModuleMember -Function prompt
Export-ModuleMember -Variable ProfileDir,ModuleDir,ScriptsDir,LocalScriptsDir