﻿<#
.SYNOPSIS
    Installs the specified module.

.DESCRIPTION
    Installs the specified module in the current session's profile.

.PARAMETER Name
    The name of the module to install. The module will be imported first, if it
    hasn't been already.

.PARAMETER AllUsers
    Install the module to the profile used by all users.

.PARAMETER CurrentHost
    Install the module to the profile used by the current host.
#>
function Install-Module {
    [CmdletBinding()]
    param(
        [Parameter(Position=0, Mandatory=$True)]
        [string]$Name,

        [Parameter()]
        [switch]$AllUsers,

        [Parameter()]
        [switch]$CurrentHost
    )

    $module = Get-Module $Name -ErrorAction SilentlyContinue
    if (-not $module) {
        Import-Module $Name -ErrorAction SilentlyContinue
        $module = Get-Module $Name -ErrorAction SilentlyContinue

        if (-not $module) {
            $n = [System.IO.Path]::GetFileNameWithoutExtension($Name)
            $module = Get-Module $n -ErrorAction SilentlyContinue
        }
    }

    if (-not $module) {
        throw "Unable to find module '$Name'"
    }

    $args = @{ "AllUsers" = $AllUsers; "CurrentHost" = $CurrentHost }
    $file = Get-ProfilePath @args

    if (-not (Test-Path $file)) {
        New-Item -Path $file -ItemType File | Out-Null
    }

    $mpath = $module.Path
    $epath = [RegEx]::Escape($mpath)
    if (-not (Select-String -Path $file -Pattern "Import-Module\s($($module.Name)|$epath)")) {
        $location = Split-Path $mpath
        $mloc = Split-Path $location
        $found = $env:PSModulePath.ToLowerInvariant().Split(';').Contains($mloc.ToLowerInvariant())
        if ($found) {
            $location = $module.Name
        }
        Write-Host "Installing $location"
        "Import-Module $location" | Out-File -Append -FilePath $file
    }
}

Export-ModuleMember -Function Install-Module