<#
.SYNOPSIS
    Finds the location of the specified item, searching the $ENV:PATH if needed. 
	
.PARAMETER Name
    The path to the item to locate.
    
.PARAMETER All
    Return all locations.
    
.PARAMETER Type
    Tells whether the path is of a particular type.
    
    Valid values are:
    -- Container: An element that contains other elements, such as a directory or registry key.
    
    -- Leaf: An element that does not contain other elements, such as a file.
    
    -- Any: Either a container or a leaf.
	
.EXAMPLE
    PS> Find-Path notepad.exe
#>
function Find-Location {
	[CmdletBinding(
		SupportsShouldProcess=$False,
		SupportsTransactions=$False,
		ConfirmImpact="None",
		DefaultParameterSetName="")]
	param(
		[Parameter(Mandatory=$True)]
		$Name,

        [Parameter(Mandatory=$False)]
        $PathName = "Path",

        [Parameter(Mandatory=$False)]
        [System.EnvironmentVariableTarget]$Target = "Process",
        
        [Parameter()]
        [switch]$All=$False,
        
        [Parameter()]
        [Microsoft.Powershell.Commands.TestPathType]$Type="Any"
	)
	
    if (Test-Path $Name -Type $Type) {
        return (Resolve-Path $Name)
    } else {
        $path = [System.Environment]::GetEnvironmentVariable($PathName, $Target)
        $paths = "$pwd;$path".split(";")
        $paths = [object[]](join-path $paths $(split-path $Name -leaf) | ? { Test-Path $_ -Type $Type })
        if ($paths.Count -gt 0) {
            if ($All) {
                return $paths
            } else {
                return $paths[0]
            }
        }
    }
    throw "Couldn't find a matching path for '$Name' of type '$Type'."
}

Export-ModuleMember -Function Find-Location
