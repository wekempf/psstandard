function New-Playlist {
	<#
	.SYNOPSIS
	Generate a Zune playlist for the MP3 files found recursively in the specified folder.

	.DESCRIPTION
	Generate a Zune playlist for the MP3 files found recursively in the specified folder.

	.PARAMETER InputPath
	The input path to search for MP3 files. Defaults to the current directory.

	.PARAMETER Name
	The name to give the playlist. Defaults to the directory name of the InputPath.
    
    .PARAMETER Public
    Make the playlist public.
	#>

	param(
		[string]$InputPath = (Get-Location),
		[string]$Name,
        [switch]$Public)

    if (-not $Name) {
        $Name = Split-Path -Leaf $InputPath
    }
    
    if ($Public) {
        $music = Get-SpecialFolder CommonMusic
    } else {
        $music = Get-SpecialFolder MyMusic
    }
    
    $playlists = join-path $music Playlists
    $media = gci -include *.mp3 -recurse $InputPath
    $fullName = (join-path $playlists $Name) + ".zpl"
    
    Write-Host "Creating playlist '$fullName'..."
    
    $xml = @'
<?zpl version="2.0"?>
<smil>
    <head/>
    <body>
        <seq/>
    </body>
</smil>
'@
  
    $body = [xml]$xml
    
    $el = $body.CreateElement('guid')
    $el.InnerText = [Guid]::NewGuid().ToString()
    [void]$body.SelectSingleNode("//head").AppendChild($el)
    
    $el = $body.CreateElement('meta')
    [void]$el.SetAttribute('name', 'generator')
    [void]$el.SetAttribute('content', 'New-Playlist')
    [void]$body.SelectSingleNode("//head").AppendChild($el)
    
    $el = $body.CreateElement('meta')
    [void]$el.SetAttribute('name', 'itemCount')
    [void]$el.SetAttribute('content', $media.Count.ToString())
    [void]$body.SelectSingleNode("//head").AppendChild($el)
    
    foreach ($file in $media) {
        $info = Get-MediaInfo $file
        $el = $body.CreateElement('media')
        [void]$el.SetAttribute('src', $file.FullName)
        [void]$el.SetAttribute('albumTitle', $info.Tag.Album)
        [void]$el.SetAttribute('albumArtist', $info.Tag.FirstAlbumArtist)
        [void]$el.SetAttribute('trackTitle', $info.Tag.Title)
        [void]$el.SetAttribute('trackArtist', $info.Tag.FirstArtist)
        [void]$el.SetAttribute('duration', $info.Properties.Duration.TotalMilliseconds)
        [void]$body.SelectSingleNode("//body/seq").AppendChild($el)
        if ($totalDuration) {
            $totalDuration = $totalDuration.Add($info.Properties.Duration)
        } else {
            $totalDuration = $info.Properties.Duration
        }
    }
    
    $el = $body.CreateElement('meta')
    [void]$el.SetAttribute('name', 'totalDuration')
    [void]$el.SetAttribute('content', ([long]$totalDuration.TotalSeconds).ToString())
    [void]$body.SelectSingleNode("//head").AppendChild($el)
    
    $el = $body.CreateElement('title')
    $el.InnerText = $Name
    [void]$body.SelectSingleNode("//head").AppendChild($el)
    
    if (-not (test-path $playlists)) {
        [void](new-item -type Directory $playlists)
    }
    
    $body.Save($fullName)
}

Export-ModuleMember -Function New-Playlist