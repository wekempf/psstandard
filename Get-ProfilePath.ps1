<#
.SYNOPSIS
    Gets the path to the specified profile configuration script.

.PARAMETER AllUsers
    Gets the path to the profile associated with all users.

.PARAMETER CurrentHost
    Gets the path to the profile associated with the current host environment.

.EXAMPLE
    PS> Get-ProfilePath

    DESCRIPTION
    -----------
    Gets the profile path for the current user and all PowerShell host environments.
#>
function Get-ProfilePath {
    [CmdletBinding()]
    param(
        [Parameter()]
        [switch]$AllUsers,

        [Parameter()]
        [switch]$CurrentHost
    )

    if ($AllUsers) {
        if ($CurrentHost) {
            $file = $profile.AllUsersCurrentHost
        } else {
            $file = $profile.AllUsersAllHosts
        }
    } else {
        if ($CurrentHost) {
            $file = $profile.CurrentUserCurrentHost
        } else {
            $file = $profile.CurrentUserAllHosts
        }
    }

    $file
}

Export-ModuleMember -Function Get-ProfilePath