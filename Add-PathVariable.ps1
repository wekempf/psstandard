<#
.SYNOPSIS
    Adds the specified paths to the end of the named, path-oriented environment variable.

.DESCRIPTION
    Adds the specified paths to the end of the named, path-oriented environment variable by taking the paths specified
    by the Value parameter and concatenating them into a semi-colon separated string. The paths can be prepended to
    the environment variable by using the -Prepend switch parameter. If a specified path is already found in the
    path-oriented environment variable it won't be added again.

.PARAMETER Value
    The paths to concat together with semi-colon separators.

.PARAMETER Name
    The name of the environment variable to add to. Typically either Path (default), LIb, Include, etc.

.PARAMETER Prepend
    The specified paths will be prepended to the environment variable instead of appended.

.PARAMETER Target
    Specifies which target scope to modify. The valid values are Process (default), User or Machine. Using
    either the User or the Machine target scope will cause the new value to persist.
#>
function Add-PathVariable {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string[]]$Value,

        [Parameter(Mandatory=$False)]
        [string]$Name = "Path",

        [Parameter()]
        [switch]$Prepend,

        [Parameter(Mandatory=$False)]
        [System.EnvironmentVariableTarget]$Target = "Process"
    )

    Begin {
        trap { ; break }
    }

    Process {
        $path = [System.Environment]::GetEnvironmentVariable($Name, $Target)
        foreach ($pathToAdd in $Value) {
            if (-not ($path.Split(";") -contains $pathToAdd)) {
                if ($Prepend) {
                    $path = "$pathToAdd;$path"
                } else {
                    $path += ";$pathToAdd"
                }
            }
        }

        [System.Environment]::SetEnvironmentVariable($Name, $path, $Target)
    }
}

Export-ModuleMember -Function Add-PathVariable