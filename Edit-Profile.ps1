<#
.SYNOPSIS
    Edits the specified profile configuration script.

.PARAMETER AllUsers
    Edits the profile associated with all users.

.PARAMETER CurrentHost
    Edits the profile associated with the current host environment.

.EXAMPLE
    PS> Edit-Profile

    DESCRIPTION
    -----------
    Edits the profile for the current user and all PowerShell host environments.
#>
function Edit-Profile {
    [CmdletBinding()]
    param(
        [Parameter()]
        [switch]$AllUsers,

        [Parameter()]
        [switch]$CurrentHost
    )

    $args = @{ "AllUsers" = $AllUsers; "CurrentHost" = $CurrentHost }
    $file = Get-ProfilePath @args
    if (-not (Test-Path $file)) {
        Write-Warning "'$file' not found."
        $opt = Get-Option ('Yes',"Create '$file'"),('No',"Do not create '$file'") -Message "Create '$file'?" -DefaultChoice 0
        if ($opt -ne 'Yes') {
            return;
        }
        New-Item -ItemType File $file | Out-Null
    }

    Edit-File $file
}

Set-Alias -Name pro -Value Edit-Profile
Export-ModuleMember -Function Edit-Profile -Alias pro