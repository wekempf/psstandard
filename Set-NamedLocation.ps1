<#
.SYNOPSIS
    Sets the location to a named/aliased location.

.PARAMETER Name
    The location name or alias.
#>
function Set-NamedLocation {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True,ValueFromPipelineByPropertyName=$True)]
        $Name
    )

    Begin {
        trap { ; break }
    }

    Process {
        $index = $Name.IndexOf('/')
        if ($index -lt 0) {
            $index = $Name.IndexOf('\')
        }
        if ($index -ge 0) {
            $path = $Name.Substring($index + 1)
            $Name = $Name.Substring(0, $index)
        }
        $location = $NamedLocations[$Name]
        if (-not $location) {
            throw "Location '$Name' not found."
        }
        if ($path) {
            $location = Join-Path $location $path
        }
        Set-Location $location
    }

    End {
    }
}

Set-Alias -Name go -Value Set-NamedLocation
Export-ModuleMember -Function Set-NamedLocation -Alias go